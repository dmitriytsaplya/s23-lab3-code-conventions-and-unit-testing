import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.ThreadController;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

public class ThreadControllerTests {

    private static final Integer ID_1 = 1;
    private static final Integer ID_2 = 2;

    private ThreadController threadController;

    private User user;
    private Post post;
    private Thread thread;
    private Thread secondThread;

    @BeforeEach
    @DisplayName("Initialization of tests")
    void init() {
        threadController = new ThreadController();

        thread = new Thread("author", new Timestamp(1), "forum", "msg", "slug", "title", 1);
        thread.setId(ID_1);

        secondThread = new Thread("author_2", new Timestamp(1), "forum_2", "msg_2", "slug_2", "title_2", 2);
        secondThread.setId(ID_2);

        user = new User("tsaplyadmitriy", "d.tsaplya@innopolis.university", "Dmitriy Tsaplya", "about");
        post = new Post("tsaplyadmitriy", new Timestamp(1), "forum", "msg", 0, 0, false);
    }

    @Test
    @DisplayName("Testing createPost")
    void createPostTest() {

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {

            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                userDAOMock.when(() -> UserDAO.Info("tsaplyadmitriy")).thenReturn(user);
                assertEquals(thread, ThreadDAO.getThreadBySlug("slug"));
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(List.of(post)),
                        threadController.createPost("slug", List.of(post)));
            }
        }

    }

    @Test
    @DisplayName("Testing posts")
    void getPostsTest() {

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getPosts(ID_1, 5, 0, "asc", true)).thenReturn(List.of(post));

            ResponseEntity response = threadController.Posts("slug", 5, 0, "asc", true);
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(List.of(post), response.getBody());
        }

    }

    @Test
    @DisplayName("Testing thread change")
    void changeThreadTest() {

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadDAOMock.when(() -> ThreadDAO.getThreadById(ID_1)).thenReturn(secondThread);

            ResponseEntity response = threadController.change("slug", thread);
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(secondThread, response.getBody());

        }
    }

    @Test
    @DisplayName("Testing thread retrieval")
    void getThreadTest() {

        try (MockedStatic<ThreadDAO> threadDAOMocked = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMocked.when(() -> ThreadDAO.getThreadById(ID_1)).thenReturn(thread);

            ResponseEntity response = threadController.info(String.valueOf(ID_1));
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertEquals(thread, response.getBody());
        }
    }

    @Test
    @DisplayName("Testing votes creation")
    void createVoteTest() {
        final Vote vote = new Vote("tsaplyadmitriy", 1);
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                thread.setVotes(1);
                userDAOMock.when(() -> UserDAO.Info("tsaplyadmitriy")).thenReturn(user);
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread),
                        threadController.createVote("slug", vote));
            }
        }

    }

}
